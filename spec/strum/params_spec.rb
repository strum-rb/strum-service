# frozen_string_literal: true

RSpec.describe "args" do
  let(:params_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) do
        output(test_param)
      end
    end
  end

  [
    { name: "param is integer", value: 10 },
    { name: "param is hash", value: { demo: :demo } },
    { name: "param is string", value: "demo value" },
    { name: "param is boolean true", value: true },
    { name: "param is boolean false", value: false },
    { name: "param is nil", value: nil }
  ].each do |params|
    it params[:name] do
      expect(params_service_class.call({ test_param: params[:value] })).to eq(params[:value])
    end
  end
end
