# frozen_string_literal: true

RSpec.describe "failure default" do
  def call_service(service_class, input)
    service_class.call(input) do |m|
      m.success { |result| result }
      m.failure { |errors| errors }
    end
  end

  it "one key - one error" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key, :err_val)
      end
    end

    expect(call_service(service_class, {})).to eq({ err_key: [:err_val] })
  end

  it "one key - few errors" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key, :err_val1)
        add_error(:err_key, :err_val2)
      end
    end

    expect(call_service(service_class, {})).to eq({ err_key: %i[err_val1 err_val2] })
  end

  it "two keys with one error" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key1, :err_val1)
        add_error(:err_key2, :err_val2)
      end
    end

    expect(call_service(service_class, {})).to eq({ err_key1: [:err_val1], err_key2: [:err_val2] })
  end

  it "two keys with two errors" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:err_key1, :err_val1)
        add_error(:err_key1, :err_val2)
        add_error(:err_key2, :err_val1)
        add_error(:err_key2, :err_val2)
      end
    end

    expect(call_service(service_class, {})).to eq({ err_key1: %i[err_val1 err_val2], err_key2: %i[err_val1 err_val2] })
  end
end
