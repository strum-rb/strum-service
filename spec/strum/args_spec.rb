# frozen_string_literal: true

RSpec.describe "args" do
  context "input from another service" do
    test_input = [
      { name: "arg value is integer", value: 10 },
      { name: "arg value is hash", value: { demo: :demo } },
      { name: "arg value is string", value: "demo value" },
      { name: "arg value is boolean", value: true }
    ]

    context "when args methods are used" do
      let(:args_service_class) do
        Class.new do
          include Strum::Service
          define_method(:call) do
            output(test_arg)
          end
        end
      end

      test_input.each do |params|
        it params[:name] do
          expect(args_service_class.call({}, test_arg: params[:value])).to eq(params[:value])
        end
      end
    end

    context "when args keys in required and sliced" do
      let(:args_service_class) do
        Class.new do
          include Strum::Service
          define_method(:call) do
            output(test_arg)
          end

          define_method(:audit) do
            sliced(:test_arg)
            required(:test_arg)
          end
        end
      end

      test_input.each do |params|
        it params[:name] do
          expect(args_service_class.call({}, test_arg: params[:value])).to eq(params[:value])
        end
      end
    end

    context "when args keys only required and wasn't sliced" do
      let(:args_service_class) do
        Class.new do
          include Strum::Service
          define_method(:call) do
            output(test_arg)
          end

          define_method(:audit) do
            sliced(:fake)
            required(:test_arg)
          end
        end
      end

      test_input.each do |params|
        it params[:name] do
          args_service_class.call({}, test_arg: params[:value]) do |m|
            m.success { "Unexpected success result" }
            m.failure { |errors| expect(errors).to include(test_arg: [:field_must_exist]) }
          end
        end
      end
    end
  end
end
