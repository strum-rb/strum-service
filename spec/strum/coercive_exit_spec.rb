# frozen_string_literal: true

RSpec.describe "coercive exit" do
  def call_service(service_class, input)
    service_class.call(input) do |m|
      m.success { |result| result }
      m.failure { |errors| errors }
    end
  end

  context "#call" do
    it "add error" do
      service_class = Class.new do
        include Strum::Service
        define_method(:call) do
          add_error(:a, :force)
          add_error!(:a, :exit)
          add_error(:b, :exit)
        end
      end

      expect(call_service(service_class, {})).to eq({ a: %i[force exit] })
    end

    it "add errors" do
      service_class = Class.new do
        include Strum::Service
        define_method(:call) do
          add_error(:b, :force)
          add_errors!({ a: %i[force exit] })
          add_error(:b, :exit)
        end
      end

      expect(call_service(service_class, {})).to eq({ b: [:force], a: %i[force exit] })
    end

    it "required" do
      service_class = Class.new do
        include Strum::Service
        define_method(:call) do
          required!(:b)
          add_error(:b, :i_break_rules)
        end
      end

      expect(call_service(service_class, { a: 1 })).to eq({ b: [:field_must_exist] })
    end

    it "throw exception" do
      service_class = Class.new do
        include Strum::Service
        define_method(:call) do
          add_error(:b, :force)
          throw :danil
        end
      end

      expect { call_service(service_class, {}) }.to throw_symbol(:danil)
    end
  end

  context "#audit" do
    let(:default_out) { { key: :value } }

    it "add error" do
      service_class = Class.new do
        include Strum::Service

        define_method(:call) { output(default_out) }

        define_method(:audit) do
          add_error(:a, :force)
          add_error!(:a, :exit)
          add_error(:b, :exit)
        end
      end

      expect(call_service(service_class, {})).to eq({ a: %i[force exit] })
    end

    it "add errors" do
      service_class = Class.new do
        include Strum::Service

        define_method(:call) { output(default_out) }

        define_method(:audit) do
          add_error(:b, :force)
          add_errors!({ a: %i[force exit] })
          add_error(:b, :exit)
        end
      end

      expect(call_service(service_class, {})).to eq({ b: [:force], a: %i[force exit] })
    end

    it "required" do
      service_class = Class.new do
        include Strum::Service

        define_method(:call) { output(default_out) }

        define_method(:audit) do
          required!(:b)
          add_error(:b, :i_break_rules)
        end
      end

      expect(call_service(service_class, { a: 1 })).to eq({ b: [:field_must_exist] })
    end

    it "throw exception" do
      service_class = Class.new do
        include Strum::Service

        define_method(:call) { output(default_out) }

        define_method(:audit) do
          add_error(:b, :force)
          throw :danil
        end
      end

      expect { call_service(service_class, {}) }.to throw_symbol(:danil)
    end
  end
end
